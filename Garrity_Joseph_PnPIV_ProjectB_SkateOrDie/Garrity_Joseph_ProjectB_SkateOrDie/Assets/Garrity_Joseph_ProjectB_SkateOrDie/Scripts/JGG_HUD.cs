﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JGG_HUD : MonoBehaviour {

    [SerializeField]
    GameObject scoreUI;

    private string scoreText;

    [SerializeField]
    GameObject SkateorDie;

    [SerializeField]
    Image timerBar;

    [SerializeField]
    GameObject bees;

    int score = 0;

    [SerializeField] bool countDown = true;
    [SerializeField] bool countReset = false;

    private float maxTime = 1.0f;

    private float minTime = 0.0f;

    [SerializeField]
    float scalingSpeed = 0.2f;

    private bool canCall = true;

    // Use this for initialization
    void Start () {
        scoreText = scoreUI.GetComponent<Text>().text;
    }
	
	// Update is called once per frame
	void Update () {
        TimeDown();
	}

    public void ModScore(int _value)
    {
        score += _value;

        UpdateScore(score);
    }

    public void UpdateScore(int value)
    {
        scoreUI.GetComponent<Text>().text = scoreText;

        scoreUI.GetComponent<Text>().text += value;
    }

    public void TimeDown()
    {
        Vector3 timerLength = timerBar.rectTransform.localScale;

        if (countDown)
        {
            timerLength.x -= scalingSpeed * Time.deltaTime;
            if (timerLength.x <= minTime)
            {
                timerLength.x = minTime;
                SkateorDie.SetActive(false);
            }
            if (timerLength.x <= 0.25f)
            {
                if (canCall == true)
                {
                    CallTheBees();
                    SkateorDie.SetActive(true);
                    canCall = false;
                }
            }
            if (countReset)
            {
                timerLength.x = maxTime;
                canCall = true;
                countReset = false;
            }
        }

        timerBar.rectTransform.localScale = timerLength;
    }

    public void CallTheBees()
    {
        Instantiate(bees);
    }

    public void ToggleTimer()
    {
        countDown = !countDown;
    }

    public void ResetTimer()
    {
        countReset = !countReset;
    }


}
