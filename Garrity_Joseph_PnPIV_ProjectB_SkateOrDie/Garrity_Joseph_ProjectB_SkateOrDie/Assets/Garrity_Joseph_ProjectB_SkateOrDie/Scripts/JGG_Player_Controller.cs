﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//------------------------------------------------------------------------------------------------ 
// Author:  Joseph Garrity 
// Date:    08-12-2017 
// Credit:  Programming Foundations 2 course
// Purpose:
//------------------------------------------------------------------------------------------------ 
public class JGG_Player_Controller : MonoBehaviour {

    [SerializeField]
    JGG_HUD HUDControl;

    private Vector3 move = Vector3.zero;

    [SerializeField]
    float moveSpeed = 5.0f;

    [SerializeField]
    float jumpVelocity = 500.0f;

    bool isGrounded = false;

    bool canSlide = false;

    bool fell = false;

    public bool Fell
    {
        get
        {
            return fell;
        }

        set
        {
            fell = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Fell)
        {
            Debug.Log("The Player has to press 'R' to reorient themsevles");
        }
        else
        {
            UpdateMovement();
            UpdateJumping();
        }

        Reposition();
    }

    void UpdateMovement()
    {
        if (canSlide)
        {
            move.x = Input.GetAxis("Horizontal") * Time.deltaTime * -moveSpeed;
        }
        else
        {
            move.x = 0.0f;
        }

        move.z = Input.GetAxis("Vertical") * Time.deltaTime * -moveSpeed;

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(transform.up, Time.deltaTime * -90.0f);
        }

        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(transform.up, Time.deltaTime * 90.0f);
        }

        transform.Translate(move);
    }

    void UpdateJumping()
    {
        if (isGrounded)
        {
            //upVelocity = -gforce * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                gameObject.GetComponent<Rigidbody>().AddForce(0, jumpVelocity, 0);
                isGrounded = false;
                //ModScore is here just for the mean time until tricks are added in remove when that happens.
                HUDControl.ModScore(200);
                //canSlide = true;
            }
        }

        GetComponent<Rigidbody>().velocity = new Vector3(0, gameObject.GetComponent<Rigidbody>().velocity.y);
    }

    void Reposition()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Floor")
        {
            isGrounded = true;
            canSlide = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Floor")
        {
            canSlide = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Floor")
        {
            canSlide = true;
        }
    }

    
}


