﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class JGG_Rider_Movement : MonoBehaviour {

    [SerializeField]
    Transform[] targets;

    NavMeshAgent navAgent;

    int arrayCount = 0;

    bool canMove = true;

	// Use this for initialization
	void Start () {
        navAgent = GetComponent<NavMeshAgent>();
	}

    // Update is called once per frame
    void Update() {
        if (canMove)
        {
            navAgent.SetDestination(targets[arrayCount].position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            arrayCount++;
            Debug.Log(this.gameObject.name + " has hit " + other.tag);
        }
        if (arrayCount >= targets.Length)
        {
            arrayCount = 0;
        }
        if(other.tag == "Player")
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
            canMove = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            gameObject.GetComponent<Renderer>().material.color = Color.white;
            canMove = true;
        }
    }
}
