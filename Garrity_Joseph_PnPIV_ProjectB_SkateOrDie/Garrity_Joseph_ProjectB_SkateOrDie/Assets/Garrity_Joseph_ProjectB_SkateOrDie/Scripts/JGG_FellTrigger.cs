﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JGG_FellTrigger : MonoBehaviour {

    [SerializeField]
    JGG_Player_Controller player;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Floor")
        {
            player.GetComponent<JGG_Player_Controller>().Fell = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Floor")
        {
            player.GetComponent<JGG_Player_Controller>().Fell = false;
        }
    }
}
