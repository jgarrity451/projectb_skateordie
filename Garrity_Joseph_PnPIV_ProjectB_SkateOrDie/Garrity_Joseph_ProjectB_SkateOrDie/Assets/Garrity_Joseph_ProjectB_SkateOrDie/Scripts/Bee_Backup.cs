﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bee_Backup : MonoBehaviour {

    enum BeeActionStates
    {
        TURNING,
        FOLLOWING
    }

    private BeeActionStates curState = BeeActionStates.TURNING;

    private Dictionary<BeeActionStates, Action> basm = new Dictionary<BeeActionStates, Action>();

    [SerializeField]
    JGG_Player_Controller player;

    [SerializeField]
    float flySpeed = 5.0f;

    // Use this for initialization
    void Start()
    {
        basm.Add(BeeActionStates.FOLLOWING, new Action(FollowingPlayer));
        basm.Add(BeeActionStates.TURNING, new Action(TurnBees));

        SetStates(BeeActionStates.TURNING);
    }

    // Update is called once per frame
    void Update()
    {
        DetermineState();

        basm[curState].Invoke();
    }

    void FollowingPlayer()
    {
        transform.position += transform.up * Time.deltaTime * flySpeed;
    }

    void TurnBees()
    {
        Vector3 playerDirection = player.transform.position - transform.position;

        transform.up = Vector3.RotateTowards(transform.up, playerDirection, Time.deltaTime * flySpeed, 0.0f);

        FollowingPlayer();
    }

    void DetermineState()
    {
        Vector3 playerDirection = transform.position - player.transform.position;

        Vector3 dirPlayerNorm = playerDirection.normalized;

        float placement = Vector3.Dot(transform.forward, dirPlayerNorm);

        float angle = Mathf.Acos(placement);

        angle = angle * Mathf.Rad2Deg;

        bool canSee = CanSeePlayer(player);

        if (canSee)
        {
            SetStates(BeeActionStates.FOLLOWING);
        }
        else if (placement > 0 && angle < 90)
        {
            SetStates(BeeActionStates.TURNING);
        }
        else
        {
            SetStates(BeeActionStates.TURNING);
        }
    }

    void SetStates(BeeActionStates _newState)
    {
        curState = _newState;
    }

    bool CanSeePlayer(JGG_Player_Controller _player)
    {
        RaycastHit senseInfo;

        bool hitPlayer = Physics.Raycast(transform.position, transform.up, out senseInfo);

        if (hitPlayer)
        {
            if (senseInfo.collider.gameObject == _player)
            {
                return true;
            }
        }

        return false;
    }
}
