﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//------------------------------------------------------------------------------------------------ 
// Author:  Joseph Garrity 
// Date:    08-12-2017 
// Credit:  Game Development Course
// Credit:  Programming Foundations 2 Course
// Purpose: 
//------------------------------------------------------------------------------------------------ 


public class JGG_Bees_Controller : MonoBehaviour {

    NavMeshAgent nav;

    Transform player;

    JGG_HUD timer;

    [SerializeField]
    float flySpeed = 5.0f;

    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        timer = GameObject.FindObjectOfType<JGG_HUD>();
    }

    void Update()
    {
        nav.SetDestination(player.position);
        nav.speed = flySpeed;
    }



    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            timer.GetComponent<JGG_HUD>().ResetTimer();
            Destroy(gameObject);
        }
    }
}
